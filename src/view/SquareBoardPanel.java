package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.*;
import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			Piece pawn = new Pawn("icon/Black P_48x48.png", false);
			this.squareControl.getSquare(1, i).setPiece(pawn);
		}
		
		Piece blackLeftRook = new Rook("icon/Black R_48x48.png", false );
		Piece blackRightRook = new Rook("icon/Black R_48x48.png", false );
		this.squareControl.getSquare(0, 0).setPiece(blackLeftRook);
		this.squareControl.getSquare(0, 7).setPiece(blackRightRook);

		Piece leftBlackKnight = new Knight("icon/Black N_48x48.png", false);
		Piece rightBlackKnight = new Knight("icon/Black N_48x48.png", false);
		this.squareControl.getSquare(0, 1).setPiece(leftBlackKnight);
		this.squareControl.getSquare(0, 6).setPiece(rightBlackKnight);

		Piece blackLeftBishop = new Bishop("icon/Black B_48x48.png", false);
		Piece blackRightBishop = new Bishop("icon/Black B_48x48.png", false);
		this.squareControl.getSquare(0, 2).setPiece(blackLeftBishop);
		this.squareControl.getSquare(0, 5).setPiece(blackRightBishop);

		Piece blackQueen = new Queen("icon/Black Q_48x48.png", false);
		this.squareControl.getSquare(0, 4).setPiece(blackQueen);
		
		Piece blackKing = new King("icon/Black K_48x48.png", false);
		this.squareControl.getSquare(0, 3).setPiece(blackKing);
		
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			Piece pawn = new Pawn("icon/White P_48x48.png", true);
			this.squareControl.getSquare(6, i).setPiece(pawn);
		}
		
		Piece whiteLeftRook = new Rook("icon/White R_48x48.png", true);
		Piece whiteRightRook = new Rook("icon/White R_48x48.png", true);
		this.squareControl.getSquare(7, 0).setPiece(whiteLeftRook);
		this.squareControl.getSquare(7, 7).setPiece(whiteRightRook);
		
		Piece leftWhiteKnight = new Knight("icon/White N_48x48.png", true);
		Piece rightWhiteKnight = new Knight("icon/White N_48x48.png", true);
		this.squareControl.getSquare(7, 1).setPiece(leftWhiteKnight);
		this.squareControl.getSquare(7, 6).setPiece(rightWhiteKnight);

		Piece whiteLeftBishop = new Bishop("icon/White B_48x48.png", true);
		Piece whiteRightBishop = new Bishop("icon/White B_48x48.png", true);
		this.squareControl.getSquare(7, 2).setPiece(whiteLeftBishop);
		this.squareControl.getSquare(7, 5).setPiece(whiteRightBishop);

		Piece whiteQueen = new Queen("icon/White Q_48x48.png", true);
		this.squareControl.getSquare(7, 4).setPiece(whiteQueen);
	
		Piece whiteKing = new King("icon/White K_48x48.png", true);
		this.squareControl.getSquare(7, 3).setPiece(whiteKing);

	}
}

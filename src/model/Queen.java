package model;

import java.awt.Point;
import java.util.ArrayList;

import control.SquareControl;

public class Queen extends Piece {
	
	public Queen(String imagePath, boolean isWhite) {
		super(imagePath, isWhite);
	}
	
	@Override
	public ArrayList<Point> possibleMoves(Square square, ArrayList<Square> squareList){
		ArrayList<Point> possibleMoves = new ArrayList<Point>();
		Point point = square.getPosition();
		Point current;
		
		for(int i = point.y+1; i < SquareControl.COL_NUMBER; i++){
			if(!squareList.get((point.x * 8) + i).havePiece()){
				current = new Point(point.x, i);
				possibleMoves.add(current);
			}
			else{
				if(squareList.get((point.x * 8) + i).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x, i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
				}
			}
		}
		
		for(int i = point.y-1; i>=0; i--){
			if(!squareList.get((point.x * 8) + i).havePiece()){
				current = new Point(point.x, i);
				possibleMoves.add(current);
			}
			else{
				if(squareList.get((point.x * 8) + i).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x, i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
				}
			}
		}
		
		for (int i = point.x+1; i < SquareControl.COL_NUMBER; i++){
			if(!squareList.get((i * 8) + point.y).havePiece()){
				current = new Point(i, point.y);
				possibleMoves.add(current);
			}
			else{
				if(squareList.get((i * 8) + point.y).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(i, point.y);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
				}
			}
		}
		
		for (int i = point.x-1;i>=0; i--){
			if(!squareList.get((i * 8) + point.y).havePiece()){
				current = new Point(i, point.y);
				possibleMoves.add(current);
			}
			else{
				if(squareList.get((i * 8) + point.y).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(i, point.y);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
				}
			}
		}
		
		int i = 1;
		while(true){
			if(point.x+i > 7 || point.y+i > 7){
				break;
			}
			
			if(squareList.get((point.x+i)*8 + (point.y+i)).havePiece()){
				if(squareList.get((point.x+i)*8 + (point.y+i)).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x+i, point.y+i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
			}
			
		}
			current = new Point(point.x+i, point.y+i);
			possibleMoves.add(current);
			i++;
		}
	
		i = 1;
		while(true){
			if(point.x+i > 7 || point.y-i < 0){
				break;
			}
			
			if(squareList.get((point.x+i)*8 + (point.y-i)).havePiece()){
				if(squareList.get((point.x+i)*8 + (point.y-i)).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x+i, point.y-i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
			}
			
		}
			current = new Point(point.x+i, point.y-i);
			possibleMoves.add(current);
			i++;
		}
		
		i = 1;
		while(true){
			if(point.x-i < 0 || point.y+i > 7){
				break;
			}
			
			if(squareList.get((point.x-i)*8 + (point.y+i)).havePiece()){
				if(squareList.get((point.x-i)*8 + (point.y+i)).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x-i, point.y+i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
			}
			
		}
			current = new Point(point.x-i, point.y+i);
			possibleMoves.add(current);
			i++;
		}
		
		i = 1;
		while(true){
			if(point.x-i < 0 || point.y-i < 0){
				break;
			}
			
			if(squareList.get((point.x-i)*8 + (point.y-i)).havePiece()){
				if(squareList.get((point.x-i)*8 + (point.y-i)).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x-i, point.y-i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
			}
			
		}
			current = new Point(point.x-i, point.y-i);
			possibleMoves.add(current);
			i++;
		}
		
		
		
		
		
		return possibleMoves;
	}
}

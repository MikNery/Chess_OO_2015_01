package model;

import java.awt.Point;
import java.util.ArrayList;

public class Bishop extends Piece{
	
	public Bishop(String imagePath, boolean isWhite) {
		super(imagePath, isWhite);
	}
	
	@Override
	public ArrayList<Point> possibleMoves(Square square, ArrayList<Square> squareList){
		ArrayList<Point> possibleMoves = new ArrayList<Point>();
		Point point = square.getPosition();
		Point current;

		int i = 1;
		while(true){
			if(point.x+i > 7 || point.y+i > 7){
				break;
			}
			
			if(squareList.get((point.x+i)*8 + (point.y+i)).havePiece()){
				if(squareList.get((point.x+i)*8 + (point.y+i)).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x+i, point.y+i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
			}
			
		}
			current = new Point(point.x+i, point.y+i);
			possibleMoves.add(current);
			i++;
		}
		
		i = 1;
		while(true){
			if(point.x+i > 7 || point.y-i < 0){
				break;
			}
			
			if(squareList.get((point.x+i)*8 + (point.y-i)).havePiece()){
				if(squareList.get((point.x+i)*8 + (point.y-i)).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x+i, point.y-i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
			}
			
		}
			current = new Point(point.x+i, point.y-i);
			possibleMoves.add(current);
			i++;
		}
		
		i = 1;
		while(true){
			if(point.x-i < 0 || point.y+i > 7){
				break;
			}
			
			if(squareList.get((point.x-i)*8 + (point.y+i)).havePiece()){
				if(squareList.get((point.x-i)*8 + (point.y+i)).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x-i, point.y+i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
			}
			
		}
			current = new Point(point.x-i, point.y+i);
			possibleMoves.add(current);
			i++;
		}
		
		i = 1;
		while(true){
			if(point.x-i < 0 || point.y-i < 0){
				break;
			}
			
			if(squareList.get((point.x-i)*8 + (point.y-i)).havePiece()){
				if(squareList.get((point.x-i)*8 + (point.y-i)).getPiece().isWhite() != square.getPiece().isWhite()){
					current = new Point(point.x-i, point.y-i);
					possibleMoves.add(current);
					break;
				}
				else{
					break;
			}
			
		}
			current = new Point(point.x-i, point.y-i);
			possibleMoves.add(current);
			i++;
		}
		
		return possibleMoves;
	}

}

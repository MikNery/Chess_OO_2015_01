package model;

import control.SquareControl;

import java.awt.Point;
import java.util.ArrayList;

public class Rook extends Piece {
	
	private ArrayList<Point> squareList;

	
	
	public Rook(String imagePath, boolean isWhite) {
		super(imagePath, isWhite);
	}
	
	public SquareControl squareControl;
	
	public ArrayList<Point> getSituation(ArrayList<Point> squareList){
		this.squareList = squareList;
		
			return this.squareList;
	}
	
	@Override
	public ArrayList<Point> possibleMoves(Square square, ArrayList<Square> squareList){
		ArrayList<Point> possibleMoves = new ArrayList<Point>();
		Point point = square.getPosition();
		Point current;
			
			for(int i = point.y+1; i < SquareControl.COL_NUMBER; i++){
				if(!squareList.get((point.x * 8) + i).havePiece()){
					current = new Point(point.x, i);
					possibleMoves.add(current);
				}
				else{
					if(squareList.get((point.x * 8) + i).getPiece().isWhite() != square.getPiece().isWhite()){
						current = new Point(point.x, i);
						possibleMoves.add(current);
						break;
					}
					else{
						break;
					}
				}
			}
			
			for(int i = point.y-1; i>=0; i--){
				if(!squareList.get((point.x * 8) + i).havePiece()){
					current = new Point(point.x, i);
					possibleMoves.add(current);
				}
				else{
					if(squareList.get((point.x * 8) + i).getPiece().isWhite() != square.getPiece().isWhite()){
						current = new Point(point.x, i);
						possibleMoves.add(current);
						break;
					}
					else{
						break;
					}
				}
			}
			
			for (int i = point.x+1; i < SquareControl.COL_NUMBER; i++){
				if(!squareList.get((i * 8) + point.y).havePiece()){
					current = new Point(i, point.y);
					possibleMoves.add(current);
				}
				else{
					if(squareList.get((i * 8) + point.y).getPiece().isWhite() != square.getPiece().isWhite()){
						current = new Point(i, point.y);
						possibleMoves.add(current);
						break;
					}
					else{
						break;
					}
				}
			}
			
			for (int i = point.x-1;i>=0; i--){
				if(!squareList.get((i * 8) + point.y).havePiece()){
					current = new Point(i, point.y);
					possibleMoves.add(current);
				}
				else{
					if(squareList.get((i * 8) + point.y).getPiece().isWhite() != square.getPiece().isWhite()){
						current = new Point(i, point.y);
						possibleMoves.add(current);
						break;
					}
					else{
						break;
					}
				}
			}
				
			
			return possibleMoves;
	}
}

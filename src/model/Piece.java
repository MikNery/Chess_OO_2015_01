package model;

import java.awt.Point;
import java.util.ArrayList;

public abstract class Piece {
	
	private String imagePath;
	private boolean isWhite;
	
	
	
	public Piece() {
	}

	public Piece(String imagePath, boolean isWhite) {
		this.imagePath = imagePath;
		this.isWhite = isWhite;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	public boolean isWhite() {
		return isWhite;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public ArrayList<Point> possibleMoves(Square square, ArrayList<Square> squareList){
		return null; 
	}
}

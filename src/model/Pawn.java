package model;

import java.awt.Point;
import java.util.ArrayList;

public class Pawn extends Piece {

	public Pawn(String imagePath, boolean isWhite) {
		super(imagePath, isWhite);
	}
	
	@Override
	public ArrayList<Point> possibleMoves(Square square, ArrayList<Square> squareList){
		ArrayList<Point> possibleMoves = new ArrayList<Point>();
		Point point = square.getPosition();
		Point current;
		
			if(square.getPiece().isWhite()){
				if(point.x == 6){
					for(int i = point.x-1; i >= 4; i--){
						if(!squareList.get((i*8)+point.y).havePiece()){
							current = new Point(i,point.y);
							possibleMoves.add(current);
						}
						else{
							break;
						}
					}
				}
				else{
						for(int i = point.x-1; i>=point.x-1; i--){
							if(!squareList.get((i*8) +point.y).havePiece()){
								current = new Point(i,point.y);
								possibleMoves.add(current);
							}
						
							else{
								break;
							}
						}
				}
			}
				/*
				 && !squareList.get((point.x-1*8) + point.y).havePiece() && !squareList.get((point.x-2*8) + point.y).havePiece()){
					current = new Point(point.x-1, point.y);
					possibleMoves.add(current);
					current = new Point(point.x-2, point.y);
					possibleMoves.add(current);
				}
				if(!squareList.get(((point.x-1)*8) + (point.y+1)).getPiece().isWhite() && squareList.get(((point.x-1)) + (point.y+1)).havePiece()){
						current = new Point(point.x-1, point.y+1);
						possibleMoves.add(current);
					}
					*//*
				if(!squareList.get(((point.x-1)*8) + (point.y-1)).getPiece().isWhite() && squareList.get(((point.x-1)*8) + (point.y+1)).havePiece()){
						current = new Point(point.x-1, point.y-1);
						possibleMoves.add(current);
					}
				/*
				
				if(!squareList.get((point.x-1*8) + (point.y)).havePiece()){
					current = new Point(point.x+1, point.y);
					possibleMoves.add(current);
				}
					
			}
			*/
			if(!square.getPiece().isWhite()){
				if(point.x == 1){
					for(int i = point.x+1; i <=3; i++){
						if(!squareList.get((i*8)+point.y).havePiece()){
							current = new Point(i,point.y);
							possibleMoves.add(current);
						}
						else{
							break;
						}
					}
				}
				else{
					for(int i = point.x+1; i<=point.x+1; i++){
						if(!squareList.get((i*8) +point.y).havePiece()){
							current = new Point(i,point.y);
							possibleMoves.add(current);
						}						
						else{
							break;
						}
					}
				}
				
				int i = point.x+1;
				if(squareList.get(((i)*8) + (point.y-1)).havePiece()){
					if(squareList.get(((i)*8) + (point.y-1)).getPiece().isWhite()){
						current = new Point(i, point.y-1);
						possibleMoves.add(current);
					}
				}
				if(squareList.get(((i)*8) + (point.y+1)).havePiece()){
					if(squareList.get(((i)*8) + (point.y+1)).getPiece().isWhite()){
						current = new Point(i, point.y+1);
						possibleMoves.add(current);
					}
				}
				/*
				if(squareList.get(((point.x+1)*8) + (point.y+1)).havePiece() || squareList.get(((point.x+1)*8) + (point.y-1)).havePiece()){
					
					if(squareList.get(((point.x+1)*8) + (point.y-1)).getPiece().isWhite()){
						current = new Point(point.x+1, point.y-1);
						possibleMoves.add(current);
					}
					
					if(squareList.get(((point.x+1)*8) + (point.y+1)).getPiece().isWhite()){
						current = new Point(point.x+1, point.y+1);
						possibleMoves.add(current);
					}
					
					
					System.out.println("yeah?");
				}
				
				if(!squareList.get((point.x-1*8) + (point.y)).havePiece()){
					current = new Point(point.x+1, point.y);
					possibleMoves.add(current);
				}
					
			}
		*/
			}
			return possibleMoves;
	}
}

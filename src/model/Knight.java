package model;

import java.awt.Point;
import java.util.ArrayList;

public class Knight extends Piece{
	
	public Knight(String imagePath, boolean isWhite) {
		super(imagePath, isWhite);
	}
	
	
	public ArrayList<Point> possibleMoves(Square square, ArrayList<Square> squareList){
		ArrayList<Point> possibleMoves = new ArrayList<Point>();
		Point point = square.getPosition();
		Point current;
		int i = -1;
		
			
			while(true){
				if(point.x + i > 7 || point.x + i < 0 || point.y + 2 > 7 || i > 1){
					break;
				}
				
				if(squareList.get((point.x+i)*8 + (point.y+2)).havePiece()){
					if(squareList.get((point.x+i)*8 + (point.y+2)).getPiece().isWhite() != square.getPiece().isWhite()){
						current = new Point(point.x+i, point.y+2);
						possibleMoves.add(current);
					}					
					i = i+2;
				}
				else{
					current = new Point(point.x+i, point.y+2);
					possibleMoves.add(current);
					i = i+2;
				}
			}
			
			i = -1;
			while(true){
				if(point.x+i > 7 || point.x + i < 0 || point.y - 2 < 0 || i > 1){
					break;
				}
				
				if(squareList.get((point.x+i)*8 + (point.y-2)).havePiece()){
					if(squareList.get((point.x+i)*8 + (point.y-2)).getPiece().isWhite() != square.getPiece().isWhite()){
						current = new Point(point.x+i, point.y-2);
						possibleMoves.add(current);
					}					
					i = i+2;
				}
				else{
					current = new Point(point.x+i, point.y-2);
					possibleMoves.add(current);
					i = i+2;
				}
			}
			
			i = -1;
			while(true){
				if(point.y+i > 7 || point.y + i < 0 || point.x + 2 > 7 || i > 1){
					break;
				}
				
				if(squareList.get((point.x+2)*8 + (point.y+i)).havePiece()){
					if(squareList.get((point.x+2)*8 + (point.y+i)).getPiece().isWhite() != square.getPiece().isWhite()){
						current = new Point(point.x+2, point.y+i);
						possibleMoves.add(current);
					}					
					i = i+2;
				}
				else{
					current = new Point(point.x+2, point.y+i);
					possibleMoves.add(current);
					i = i+2;
				}
			}
			
			i = -1;
			while(true){
				if(point.y+i > 7 || point.y + i < 0 || point.x - 2 < 0 || i > 1){
					break;
				}
				
				if(squareList.get((point.x-2)*8 + (point.y+i)).havePiece()){
					if(squareList.get((point.x-2)*8 + (point.y+i)).getPiece().isWhite() != square.getPiece().isWhite()){
						current = new Point(point.x-2, point.y+i);
						possibleMoves.add(current);
					}					
					i = i+2;
				}
				else{
					current = new Point(point.x-2, point.y+i);
					possibleMoves.add(current);
					i = i+2;
				}
			}
	
			return possibleMoves;
	}

}

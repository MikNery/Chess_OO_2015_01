package control;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import model.Square;
import model.Square.SquareEventListener;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;

	private Square selectedSquare;
	private ArrayList<Square> squareList;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;

		this.squareList = new ArrayList<>();
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}
	

	@Override
	public void onHoverEvent(Square square) {
		if(square.getColor() != this.colorSelected)
			square.setColor(this.colorHover);
	}

	@Override 
	public void onSelectEvent(Square square) {
		if (haveSelectedCellPanel()) { 
			if (!this.selectedSquare.equals(square) && square.getColor().equals(this.colorSelected)) {
					moveContentOfSelectedSquare(square);
					resetBoardColor(getSquareList());
			}
			 else {
				unselectSquare(square); 
				resetBoardColor(getSquareList());
			}
		} else {
			selectSquare(square);
		}
	}
	
	
	public void colorMoves(Square square){
		ArrayList<Point> possibleMoves = square.getPiece().possibleMoves(square, getSquareList());
			for(int i = 0; i < possibleMoves.size(); i++) {
				Point point = possibleMoves.get(i);
					getSquare(point.x, point.y).setColor(this.colorSelected);
			}
	}
	
	
	@Override
	public void onOutEvent(Square square) {
		if (this.selectedSquare != square && square.getColor() != this.colorSelected) {
			resetColor(square);
		} else {
			square.setColor(this.colorSelected);
		}
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}
	
	private void resetBoardColor(ArrayList<Square> squareList){
		Square square;
		for (int i = 0; i <64; i++){
			square = squareList.get(i);
			resetColor(square);
		}	
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		square.setPiece(this.selectedSquare.getPiece());
		this.selectedSquare.removeImage();
		unselectSquare(square);
	}

	private void selectSquare(Square square) {
		if (square.havePiece()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			colorMoves(this.selectedSquare);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
}
